
/**
 *
 * @author Charles Fitzhenry
 */
public class ApplicationRecord {

    String term;
    String academicCareer;
    String program;
    String applicationNumber;
    String applicationDate;
    String applicationStatus;

    public ApplicationRecord() {
        this.term = "";
        this.academicCareer = "";
        this.program = "";
        this.applicationNumber = "";
        this.applicationDate = "";
        this.applicationStatus = "";
    }

    public ApplicationRecord(String term, String academicCareer, String program, String applicationNumber, String applicationDate, String applicationStatus) {
        this.term = term;
        this.academicCareer = academicCareer;
        this.program = program;
        this.applicationNumber = applicationNumber;
        this.applicationDate = applicationDate;
        this.applicationStatus = applicationStatus;
    }

    public String getTerm() {
        return term;
    }

    public String getAcademicCareer() {
        return academicCareer;
    }

    public String getProgram() {
        return program;
    }

    public String getApplicationNumber() {
        return applicationNumber;
    }

    public String getApplicationDate() {
        return applicationDate;
    }

    public String getApplicationStatus() {
        return applicationStatus;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public void setAcademicCareer(String academicCareer) {
        this.academicCareer = academicCareer;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public void setApplicationNumber(String applicationNumber) {
        this.applicationNumber = applicationNumber;
    }

    public void setApplicationDate(String applicationDate) {
        this.applicationDate = applicationDate;
    }

    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }
    
    

}
