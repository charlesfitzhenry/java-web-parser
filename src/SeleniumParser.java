
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.Select;

/**
 *
 * @author Charles Fitzhenry
 */
//TO RUN, THE SELENIUM WEB SERVER NEEDS TO BE STARTED UP FIRST!!
public class SeleniumParser {

    public static void main(String[] args) {

        //Open a text file containing student numbers seperated by newlines
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a filename containing student numbers: ");
        String inFileName = sc.nextLine();

        //List to store student numbers
        ArrayList<String> stds = new ArrayList<>();

        //A list to store application records
        ArrayList<Application> applications = new ArrayList<>();

        try {
            FileReader f = new FileReader(inFileName);
            BufferedReader bf = new BufferedReader(f);
            String line;
            while ((line = bf.readLine()) != null) {
                //Append to list
                if (!"".equals(line.trim())) {
                    stds.add(line.trim());
                }
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(SeleniumParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SeleniumParser.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (String studentNum : stds) {

            // Create a new instance of the html unit driver
            // Notice that the remainder of the code relies on the interface, 
            // not the implementation.
            WebDriver driver = new HtmlUnitDriver(true);

            // And now use this to visit Google
            driver.get("https://srvslspsw004.uct.ac.za/psc/public/EMPLOYEE/HRMS/c/UCT_PUBLIC_MENU.UCT_SS_ADV_PUBLIC.GBL?PortalActualURL=https%3a%2f%2fsrvslspsw004.uct.ac.za%2fpsc%2fpublic%2fEMPLOYEE%2fHRMS%2fc%2fUCT_PUBLIC_MENU.UCT_SS_ADV_PUBLIC.GBL&PortalContentURL=https%3a%2f%2fsrvslspsw004.uct.ac.za%2fpsc%2fpublic%2fEMPLOYEE%2fHRMS%2fc%2fUCT_PUBLIC_MENU.UCT_SS_ADV_PUBLIC.GBL&PortalContentProvider=HRMS&PortalCRefLabel=Public%20Access&PortalRegistryName=EMPLOYEE&PortalServletURI=https%3a%2f%2fsrvslspsw004.uct.ac.za%2fpsp%2fpublic%2f&PortalURI=https%3a%2f%2fsrvslspsw004.uct.ac.za%2fpsc%2fpublic%2f&PortalHostNode=HRMS&NoCrumbs=yes&PortalKeyStruct=yes");

            System.out.println("On student number: " + studentNum);
            //waitForPageToLoad(driver);

            // Find the text input element by its name
            WebElement element = driver.findElement(By.name("UCT_DERIVED_PUB_CAMPUS_ID"));

            // Enter student number in question
            element.sendKeys(studentNum.trim().toUpperCase());

            //Grab the dropdown element and select the desired request
            Select dropdown = new Select(driver.findElement(By.name("UCT_DERIVED_PUB_UCT_DERIVED_LINK6")));
            dropdown.selectByValue("AD"); //To view application status

            // Now submit the form. WebDriver will find the form for us from the element
            WebElement button = driver.findElement(By.name("UCT_DERIVED_PUB_SS_DERIVED_LINK"));
            button.click();
            element.submit();

            //waitForPageToLoad(driver);
            //waitSeconds(5);
            //We expect to be on the returned page now.
            //Grab all elements needed
            //Create a new Application object to store data for particular applicant
            Application app = new Application();

            //Set student number
            app.setStudentNumber(studentNum);

            //Grab name
            WebElement studentName = driver.findElement(By.id("DERIVED_SSP_DSP_PERSON_NAME"));

            //Set name in Application object
            app.setStudentName(studentName.getText());

            //Now try find all applications. 6 because we don't expect any applicant to have more than 2-4
            for (int i = 0; i < 6; i++) {

                //Get term
                try {
                    WebElement term = driver.findElement(By.id("TERM_VAL_TBL_DESCR$" + i));
                    WebElement academicCareer = driver.findElement(By.id("ACAD_CAR_TBL_DESCR$" + i));
                    WebElement program = driver.findElement(By.id("ACAD_PROG_TBL_DESCR$" + i));
                    WebElement appNumber = driver.findElement(By.id("ADM_SS_APPL_PRG_ADM_APPL_NBR$" + i));
                    WebElement appDate = driver.findElement(By.id("ADM_SS_APPL_PRG_ADM_APPL_DT$" + i));
                    WebElement appStatus = driver.findElement(By.id("UCT_DERIVED_DESCRLONG$" + i));

                    //create a new application record and add it to the application
                    app.addRecord(term.getText(), academicCareer.getText(), program.getText(), appNumber.getText(), appDate.getText(), appStatus.getText());

                } catch (Exception ex) {

                    break;
                }

            }

            //now add Application record to list
            applications.add(app);

            //Navigate back
            /* WebElement back = driver.findElement(By.id("UCT_DERIVED_PUB_DERIVED_LINK11"));
            back.click();
            back.submit();*/
            //waitSeconds(5);
            driver.quit();
        }

        //Write to file
        PrintWriter writer;
        try {
            String fileN = "Output " + LocalDateTime.now().getDayOfMonth() + "-" + LocalDateTime.now().getMonthValue() + "-" + LocalDateTime.now().getYear() + " at " + LocalDateTime.now().getHour() + "-" + LocalDateTime.now().getMinute() + "-" + LocalDateTime.now().getSecond() + ".txt";
            writer = new PrintWriter(fileN, "UTF-8");

            for (Application a : applications) {
                for (ApplicationRecord aR : a.getAppRecord()) {
                    writer.println(a.getStudentName() + "," + a.getStudentNumber() + "," + aR.getTerm() + "," + aR.getAcademicCareer() + "," + aR.getProgram() + "," + aR.getApplicationNumber() + "," + aR.getApplicationDate() + "," + aR.getApplicationStatus());
                }
            }
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(SeleniumParser.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void waitForPageToLoad(WebDriver driver) {
        String pageLoadStatus = "";
        do {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            pageLoadStatus = (String) js.executeScript("return document.readyState");
            System.out.print(".");
        } while (!pageLoadStatus.equals("complete"));
        System.out.println();
        System.out.println("Page Loaded.");
    }

    public static void waitSeconds(int secons) {
        System.out.print("Pausing for " + secons + " seconds: ");
        try {
            Thread.currentThread();
            int x = 1;
            while (x <= secons) {
                Thread.sleep(1000);
                System.out.print(" " + x);
                x = x + 1;
            }
            System.out.print('\n');
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

}
