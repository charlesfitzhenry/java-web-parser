
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlButtonInput;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlOption;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSelect;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Charles Fitzhenry
 */
public class Parser {

    public static void main(String[] args) {
        String studentNumber = "ABCDEF001";

        //System.setProperty("https.protocols", "SSLv3");
        try (final WebClient webClient = new WebClient()) {
            /* turn off annoying htmlunit warnings */
            java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(java.util.logging.Level.OFF);

            //Enable JavaScript of HTMLUnit
            //webClient.getOptions().setJavaScriptEnabled(true);
            // Get the first page
            HtmlPage page1 = webClient.getPage("https://srvslspsw004.uct.ac.za/psc/public/EMPLOYEE/HRMS/c/UCT_PUBLIC_MENU.UCT_SS_ADV_PUBLIC.GBL?PortalActualURL=https%3a%2f%2fsrvslspsw004.uct.ac.za%2fpsc%2fpublic%2fEMPLOYEE%2fHRMS%2fc%2fUCT_PUBLIC_MENU.UCT_SS_ADV_PUBLIC.GBL&PortalContentURL=https%3a%2f%2fsrvslspsw004.uct.ac.za%2fpsc%2fpublic%2fEMPLOYEE%2fHRMS%2fc%2fUCT_PUBLIC_MENU.UCT_SS_ADV_PUBLIC.GBL&PortalContentProvider=HRMS&PortalCRefLabel=Public%20Access&PortalRegistryName=EMPLOYEE&PortalServletURI=https%3a%2f%2fsrvslspsw004.uct.ac.za%2fpsp%2fpublic%2f&PortalURI=https%3a%2f%2fsrvslspsw004.uct.ac.za%2fpsc%2fpublic%2f&PortalHostNode=HRMS&NoCrumbs=yes&PortalKeyStruct=yes");

            // Get the form that we are dealing with and within that form,   
            final HtmlForm form = page1.getFormByName("win0");

            final HtmlTextInput textField = form.getInputByName("UCT_DERIVED_PUB_CAMPUS_ID");

            // Change the value of the text field
            textField.setValueAttribute(studentNumber);
            HtmlSelect select = (HtmlSelect) form.getSelectByName("UCT_DERIVED_PUB_UCT_DERIVED_LINK6");

            HtmlOption option = select.getOptionByValue("AD");

            select.setSelectedAttribute(option, true);

            // find the submit button and the field that we want to change.
            final HtmlButtonInput button = form.getInputByName("UCT_DERIVED_PUB_SS_DERIVED_LINK");

            System.out.println(page1.asXml());
            final HtmlPage page2 = button.click();
            // HtmlPage page = page2;

            System.out.println(page2.asXml());
            System.out.println(page2.asText());

        } catch (IOException | FailingHttpStatusCodeException ex) {
            Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
