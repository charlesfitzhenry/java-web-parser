
import java.util.ArrayList;

/**
 *
 * @author Charles Fitzhenry
 */
public class Application {

    String studentName;
    String studentNumber;
    ArrayList<ApplicationRecord> appRecord;

    public Application() {
        this.studentName = "";
        this.studentNumber = "";
        appRecord = new ArrayList<>();

    }

    public Application(String studentName, String studentNumber) {
        this.studentName = studentName;
        this.studentNumber = studentNumber;
        appRecord = new ArrayList<>();
    }

    public void addRecord(String term, String academicCareer, String program, String applicationNumber, String applicationDate, String applicationStatus) {
        ApplicationRecord temp = new ApplicationRecord(term, academicCareer, program, applicationNumber, applicationDate, applicationStatus);
        appRecord.add(temp);
    }

    public void addRecord(ApplicationRecord appRec) {
        appRecord.add(appRec);
    }

    public String getStudentName() {
        return studentName;
    }

    public String getStudentNumber() {
        return studentNumber;
    }

    public ArrayList<ApplicationRecord> getAppRecord() {
        return appRecord;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public void setStudentNumber(String studentNumber) {
        this.studentNumber = studentNumber;
    }

    public void setAppRecord(ArrayList<ApplicationRecord> appRecord) {
        this.appRecord = appRecord;
    }

    @Override
    public String toString() {
        return "Application{" + "studentName=" + studentName + ", studentNumber=" + studentNumber + ", appRecord=" + appRecord + '}';
    }
    
    public void writeToFile(String fileName){
        
        
    }

}
